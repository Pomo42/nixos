{
  pkgs,
  inputs,
  ...
}:
{
  imports = [
    # Nixos modules
    ./nixosModules
    # Home manager
    inputs.home-manager.nixosModules.default
  ];

  # Enabling flakes
  nix.settings.experimental-features = [
    "nix-command"
    "flakes"
  ];

  # Auto optimise to save space
  nix.settings.auto-optimise-store = true;

  # Set your time zone.
  time.timeZone = "Europe/London";

  programs.zsh.enable = true;

  # Importing home manager
  home-manager = {
    extraSpecialArgs = {
      inherit inputs;
    };
    users = {
      "amy" = import ./home.nix;
    };
    backupFileExtension = "backup";
  };

  # Enabling git
  programs.git.enable = true;
  programs.ssh.askPassword = "";

  # Defining session variables
  environment.sessionVariables = {
    FLAKE = "/home/$USER/nixos";
    #NIXOS_OZONE_WL = "1";
  };

  # GnuPG
  programs.gnupg.agent = {
    enable = true;
    pinentryPackage = pkgs.pinentry-curses;
  };

  # List packages installed in system profile. To search, run:
  environment.systemPackages = with pkgs; [
    killall
    wget
    nh
    vesktop
    keepassxc
    pulsemixer
    bc
    libreoffice
    lazygit
    # Media
    zathura
    sxiv
    mpv
    nemo
    qbittorrent
    # School work
    texliveMedium
    obsidian
    rnote
    uv
  ];
}
