{
  imports = [
    ./gaming.nix
    #./hyprland.nix
    ./stylix.nix
    ./thunderbird.nix
    #./toki-pona.nix
    ./xorg.nix
  ];
}
