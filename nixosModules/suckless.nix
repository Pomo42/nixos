{
  lib,
  config,
  pkgs,
  ...
}:
{
  nixpkgs.overlays = [
    (self: super: {
      dwm = super.dwm.overrideAttrs (oldAttrs: rec {
        patches = [
          ./patches/dwm/dwm-actualfullscreen-20191112-cb3f58a.diff
          ./patches/dwm/dwm-alwayscenter-20200625-f04cac6.diff
          ./patches/dwm/dwm-centeredwindowname-20180909-6.2.diff
          ./patches/dwm/dwm-swallow-6.3.diff
        ];
        configFile = super.writeText "config.h" (builtins.readFile ./savedconfig/dwm-6.5.h);
        postPatch =
          oldAttrs.postPatch or "" + "\necho 'Using own config file...'\n cp ${configFile} config.def.h";
      });
    })

    (self: super: {
      dmenu = super.dmenu.overrideAttrs (oldAttrs: rec {
        patches = [
          ./patches/dmenu/dmenu-border-20230512-0fe460d.diff
          ./patches/dmenu/dmenu-center-5.2.diff
        ];
        configFile = super.writeText "config.h" (builtins.readFile ./savedconfig/dmenu-5.3.h);
        postPatch =
          oldAttrs.postPatch or "" + "\necho 'Using own config file...'\n cp ${configFile} config.def.h";
      });
    })
  ];

  services.xserver.windowManager.dwm.enable = true;

  # Turning slock on
  programs.slock.enable = true;

  environment.systemPackages = with pkgs; [
    dmenu
    # Screenshotting
    scrot
    xclip
  ];
}
