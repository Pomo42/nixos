{ pkgs, ... }:
{
  imports = [
    ./suckless.nix
    ./monitor.nix
    ./nvidia.nix
  ];

  # Enable xorg
  services.xserver = {
    enable = true;
  };

  # Enable startx
  services.xserver.displayManager.startx.enable = true;

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "de";
    options = "caps:swapescape";
  };
}
