{ ... }:
{

  # Kernel parameters
  boot.kernelParams = [ "nvidia.NVreg_PreserveVideoMemoryAllocations=1" ];
  services.xserver.videoDrivers = [ "nvidia" ];

  # Nvidia settings
  hardware.nvidia = {
    modesetting.enable = true;
    open = false;
  };
}
