{ pkgs, ... }:
{
  stylix = {
    enable = true;
    base16Scheme = "${pkgs.base16-schemes}/share/themes/catppuccin-mocha.yaml";
    image = ../wallpaper.png;

    cursor = {
      package = pkgs.adwaita-icon-theme;
      name = "Adwaita";
      size = 14;
    };

    polarity = "dark";
    fonts = {
      monospace = {
        package = pkgs.liberation_ttf;
        name = "Liberation Mono";
      };

      serif = {
        package = pkgs.liberation_ttf;
        name = "Liberation Mono";
      };

      sansSerif = {
        package = pkgs.liberation_ttf;
        name = "Liberation Mono";
      };

      emoji = {
        package = pkgs.noto-fonts-emoji;
        name = "Noto Color Emoji";
      };

      sizes = {
        applications = 12;
        terminal = 14;
        desktop = 10;
        popups = 10;
      };
    };
  };
}
