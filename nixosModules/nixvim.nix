{ inputs, pkgs, ... }:
{
  programs.nixvim = {
    enable = true;
    defaultEditor = true;

    # Nerd tree
    extraPlugins = with pkgs.vimPlugins; [
      nerdtree
    ];

    # Options
    opts = {
      number = true;
      relativenumber = true;
      scrolloff = 8;
      tabstop = 4;
      shiftwidth = 4;
      ruler = true;
      ignorecase = true;
      smartcase = true;
      incsearch = true;
      smartindent = true;
      wrap = false;
      hlsearch = false;
      swapfile = false;
      mouse = "";
    };

    # Keymaps 
    globals.mapleader = " ";
    keymaps = [
      {
        mode = "n";
        key = "<C-f>";
        action = ":NERDTreeToggle<CR>";
      }
      {
        mode = "n";
        key = "<leader>o";
        action = ":setlocal spell! spelllang=en_gb<CR>";
      }
      {
        mode = "n";
        key = "<C-h>";
        action = "<C-w>h";
      }
      {
        mode = "n";
        key = "<C-j>";
        action = "<C-w>j";
      }
      {
        mode = "n";
        key = "<C-k>";
        action = "<C-w>k";
      }
      {
        mode = "n";
        key = "<C-l>";
        action = "<C-w>l";
      }
      {
        mode = "n";
        key = "Y";
        action = "y$";
      }
      {
        mode = "n";
        key = "<leader><leader>";
        action = "/<++><CR>ca<";
      }
      {
        mode = "v";
        key = "<leader>yy";
        action = ":!xclip -f -sel clip<CR>";
      }
    ];

    # Autocmd
    autoCmd = [
      {
        event = "BufWritePre";
        pattern = "*";
        command = "%s/\s\+$//e";
      }
      {
        event = "BufWritePre";
        pattern = "*";
        command = "%s/\n\+\%$//e";
      }
      {
        event = "BufWritePre";
        pattern = "*.[ch]";
        command = "%s/\%$/\r/e";
      }
      {
        event = "Filetype";
        pattern = ".rmd";
        command = ''map <F5> :!echo<space>"require(rmarkdown);<space>render('<c-r>%')"<space>\|<space>R<space>--vanilla<enter><enter>'';
      }
    ];
  };
}
