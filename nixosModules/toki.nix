{
  lib,
  stdenvNoCC,
  fetchFromGitHub,
  ibus,
  ibus-engines,
}:
stdenvNoCC.mkDerivation {
  name = "ibus-table-tokipona";

  src = fetchFromGitHub {
    owner = "Id405";
    repo = "sitelen-pona-ucsur-guide";
    rev = "36d9c456fd8f155b59c1c3e7177d586a0b8f7986";
    sha256 = "sha256-c/342zQ6XX8UVu1KWxRQJHoTomUdCH+Ub2tkN3xT50E=";
  };

  buildInputs = [
    ibus
    ibus-engines.table
  ];

  buildPhase = ''
    export HOME=$(mktemp -d)/ibus-table-tokipona
    ibus-table-createdb -s tokipona.txt -n tokipona.db
  '';

  installPhase = ''
    mkdir -p $out/share/ibus-table/tables
    cp *.db $out/share/ibus-table/tables/
  '';

  postFixup = ''
    rm -rf $HOME
  '';

  meta = with lib; {
    isIbusEngine = true;
  };
}
