{ pkgs, ... }:
{
  fonts.packages = with pkgs; [
    nasin-nanpa
  ];

  i18n.inputMethod = {
    enable = true;
    type = "ibus";
    ibus.engines = with pkgs.ibus-engines; [
      (pkgs.callPackage ./toki.nix { })
      table
    ];
  };
}
