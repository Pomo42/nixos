{ config, pkgs, ... }:
{
  services.xserver.config = ''
        Section "ServerLayout"

      # Reference the Screen sections for each driver.  This will
      # cause the X server to try each in turn.
        Identifier     "Layout[all]"
        Screen      0  "Screen0" 0 0
        InputDevice    "Keyboard0" "CoreKeyboard"
        InputDevice    "Mouse0" "CorePointer"
        Option         "Xinerama" "0"
    EndSection

    Section "ServerLayout"

      # Reference the Screen sections for each driver.  This will
      # cause the X server to try each in turn.
        Identifier     "Layout[all]"
        Screen         "Screen-nvidia[0]" 0 0
        InputDevice    "Keyboard0" "CoreKeyboard"
        InputDevice    "Mouse0" "CorePointer"
    EndSection

    Section "Module"
    EndSection

    Section "ServerFlags"
        Option         "AllowMouseOpenFail" "on"
        Option         "DontZap" "on"
    EndSection

    Section "InputDevice"

        # generated from default
        Identifier     "Keyboard0"
        Driver         "kbd"
    EndSection

    Section "InputDevice"

        # generated from default
        Identifier     "Mouse0"
        Driver         "mouse"
        Option         "Protocol" "auto"
        Option         "Device" "/dev/input/mice"
        Option         "Emulate3Buttons" "no"
        Option         "ZAxisMapping" "4 5"
    EndSection

    Section "InputClass"
        Identifier         "libinput mouse configuration"
        MatchIsPointer     "on"
        MatchDriver        "libinput"
        Option         "AccelProfile" "adaptive"
        Option         "LeftHanded" "off"
        Option         "MiddleEmulation" "on"
        Option         "NaturalScrolling" "off"
        Option         "ScrollMethod" "twofinger"
        Option         "HorizontalScrolling" "on"
        Option         "SendEventsMode" "enabled"
        Option         "Tapping" "on"
        Option         "TappingDragLock" "on"
        Option         "DisableWhileTyping" "off"
    EndSection

    Section "InputClass"
        Identifier         "libinput touchpad configuration"
        MatchIsTouchpad    "on"
        MatchDriver        "libinput"
        Option         "AccelProfile" "adaptive"
        Option         "LeftHanded" "off"
        Option         "MiddleEmulation" "on"
        Option         "NaturalScrolling" "off"
        Option         "ScrollMethod" "twofinger"
        Option         "HorizontalScrolling" "on"
        Option         "SendEventsMode" "enabled"
        Option         "Tapping" "on"
        Option         "TappingDragLock" "on"
        Option         "DisableWhileTyping" "off"
    EndSection

    Section "InputClass"
        Identifier         "libinput mouse configuration"
        MatchIsPointer     "on"
        MatchDriver        "libinput"
        Option         "AccelProfile" "adaptive"
        Option         "LeftHanded" "off"
        Option         "MiddleEmulation" "on"
        Option         "NaturalScrolling" "off"
        Option         "ScrollMethod" "twofinger"
        Option         "HorizontalScrolling" "on"
        Option         "SendEventsMode" "enabled"
        Option         "Tapping" "on"
        Option         "TappingDragLock" "on"
        Option         "DisableWhileTyping" "off"
    EndSection

    Section "InputClass"
        Identifier         "libinput touchpad configuration"
        MatchIsTouchpad    "on"
        MatchDriver        "libinput"
        Option         "AccelProfile" "adaptive"
        Option         "LeftHanded" "off"
        Option         "MiddleEmulation" "on"
        Option         "NaturalScrolling" "off"
        Option         "ScrollMethod" "twofinger"
        Option         "HorizontalScrolling" "on"
        Option         "SendEventsMode" "enabled"
        Option         "Tapping" "on"
        Option         "TappingDragLock" "on"
        Option         "DisableWhileTyping" "off"
    EndSection

    Section "Monitor"
        Identifier     "Monitor[0]"
    EndSection

    Section "Monitor"
        Identifier     "Monitor0"
        VendorName     "Unknown"
        ModelName      "Asustek Computer Inc ASUS VP279"
        HorizSync       24.0 - 84.0
        VertRefresh     48.0 - 75.0
    EndSection

    Section "Monitor"
        Identifier     "Monitor[0]"
    EndSection

    Section "Device"
        Identifier     "Device-nvidia[0]"
        Driver         "nvidia"
        Option         "SidebandSocketPath" "/run/nvidia-xdriver/"
    EndSection

    Section "Device"
        Identifier     "Device0"
        Driver         "nvidia"
        VendorName     "NVIDIA Corporation"
        BoardName      "NVIDIA GeForce GTX 1660 SUPER"
    EndSection

    Section "Device"
        Identifier     "Device-nvidia[0]"
        Driver         "nvidia"
        Option         "SidebandSocketPath" "/run/nvidia-xdriver/"
    EndSection

    Section "Screen"
        Identifier     "Screen-nvidia[0]"
        Device         "Device-nvidia[0]"
        Monitor        "Monitor[0]"
        Option         "RandRRotation" "on"
    EndSection

    Section "Screen"

    # Removed Option "metamodes" "HDMI-0: 1920x1080_75 +1920+0, DP-1: 1920x1080_60 +0+0"
        Identifier     "Screen0"
        Device         "Device0"
        Monitor        "Monitor0"
        DefaultDepth    24
        Option         "Stereo" "0"
        Option         "nvidiaXineramaInfoOrder" "HDMI-0"
        Option         "metamodes" "HDMI-0: 1920x1080_75 +1920+0 {ForceCompositionPipeline=On, ForceFullCompositionPipeline=On}, DP-1: 1920x1080_60 +0+0 {ForceCompositionPipeline=On, ForceFullCompositionPipeline=On}"
        Option         "SLI" "Off"
        Option         "MultiGPU" "Off"
        Option         "BaseMosaic" "off"
        SubSection     "Display"
            Depth       24
        EndSubSection
    EndSection

    Section "Screen"
        Identifier     "Screen-nvidia[0]"
        Device         "Device-nvidia[0]"
        Monitor        "Monitor[0]"
        Option         "RandRRotation" "on"
    EndSection
  '';
}
