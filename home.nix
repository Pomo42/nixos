{ config, pkgs, ... }:

{
  imports = [ ./homeModules ];
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "amy";
  home.homeDirectory = "/home/amy";

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = [ ];

  stylix.targets.hyprland.enable = false;
  stylix.targets.hyprpaper.enable = false;

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = { };

  home.sessionVariables = { };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  ### DO NOT CHANGE!!! ###
  home.stateVersion = "23.11";
}
