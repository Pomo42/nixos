{ pkgs, config, ... }:
{
  programs.waybar = {
    enable = true;
    settings = {
      mainBar = {
        layer = "top";
        position = "top";

        modules-left = [
          "hyprland/workspaces"
        ];

        modules-right = [
          "clock"
        ];

        "hyprland/workspaces" = {
          format = "{icon}";
          format-icons = {
            "1" = "I";
            "2" = "II";
            "3" = "III";
            "4" = "IV";
            "5" = "V";
            "11" = "I";
            "12" = "II";
            "13" = "III";
            "14" = "IV";
            "15" = "V";
          };
          on-click = "activate";
        };

        clock = {
          format = "{:%b %d (%a) %H:%M}";
        };
      };
    };
  };
}
