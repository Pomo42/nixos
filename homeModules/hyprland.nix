{ pkgs, inputs, ... }:
let
  startScript =
    pkgs.writeShellScriptBin "start" # bash
      ''
        ${pkgs.swww}/bin/swww init &
        ${pkgs.waybar}/bin/waybar &
        sleep 1
        ${pkgs.swww}/bin/swww img ${../wallpaper.png}
      '';

in
{
  wayland.windowManager.hyprland = {
    enable = true;
    package = inputs.hyprland.packages.${pkgs.system}.hyprland;

    plugins = [
      inputs.split-monitor-workspaces.packages.${pkgs.system}.split-monitor-workspaces
    ];
    settings = {
      # Setting keyboard
      input = {
        kb_layout = "de";
        kb_options = "caps:swapescape";
      };

      # Disabling animations (i don't like em)
      animations = {
        enabled = false;
      };

      # Master layout
      general.layout = "master";
      master = {
        new_is_master = true;
        mfact = 0.5;
      };

      # Window Decoration
      general = {
        gaps_in = 3;
        gaps_out = 5;
        border_size = 2;
      };
      decoration = {
        rounding = 3;
      };

      misc = {
        enable_swallow = true;
        disable_hyprland_logo = true;
      };

      # Keyboard shortcuts
      "$mod" = "SUPER";
      "$term" = "${pkgs.alacritty}/bin/alacritty";
      bind =
        [
          "$mod, d, exec, ${pkgs.wofi}/bin/wofi --show=run"
          "$mod SHIFT, return, exec, $term"
          "$mod, s, exec, ${pkgs.hyprshot}/bin/hyprshot -m region --clipboard-only"
          "$mod, m, exec, ${pkgs.hyprlock}/bin/hyprlock"
          # term center commands

          # Move focus
          "$mod, h, movefocus, l"
          "$mod, l, movefocus, r"
          "$mod, k, movefocus, u"
          "$mod, j, movefocus, d"

          # Move windows
          "$mod SHIFT, h, movewindow, l"
          "$mod SHIFT, l, movewindow, r"
          "$mod SHIFT, k, movewindow, u"
          "$mod SHIFT, j, movewindow, d"

          "$mod, c, killactive"
          "$mod, space, fullscreen"
        ]
        ++ map (n: "$mod, ${toString n}, split-workspace, ${toString n}") [
          1
          2
          3
          4
          5
        ]
        ++ map (n: "$mod SHIFT, ${toString n}, split-movetoworkspacesilent, ${toString n}") [
          1
          2
          3
          4
          5
        ];

      # Mouse movements
      bindm = [
        "$mod, mouse:272, movewindow"
        "$mod, mouse:273, resizewindow"
      ];

      exec-once = [
        "${pkgs.bash}/bin/bash ${startScript}/bin/start"
      ];
    };
  };

  imports = [
    # Bar
    ./waybar.nix
    # Locking
    ./hyprlock.nix
  ];

  home.packages = with pkgs; [
    # Application launcher
    wofi

    # Screenshotting
    hyprshot
    wl-clipboard

    # Wallpaper
    swww
  ];
}
