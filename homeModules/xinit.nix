{
  pkgs,
  ...
}:
{
  home.file.".xinitrc" = {
    enable = true;
    text = ''
      ${pkgs.feh}/bin/feh --bg-fill ${../wallpaper.png} &
      while :
      do
          ${pkgs.xorg.xsetroot}/bin/xsetroot -name "$(date +'%b %d (%a) %H:%M')"
          sleep 5
      done &

      exec dwm
    '';
  };
}
