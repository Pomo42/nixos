{ pkgs, lib, ... }:
{
  programs.hyprlock = {
    enable = true;
    settings = {
      general = {
        disable_loading_bar = true;
      };

      background = [
        {
          path = "${../wallpaper.png}";
          blur_passes = 4;
          blur_size = 5;
        }
      ];

      input-field = [
        {
          size = "600, 50";
          outline_thickness = 3;
          dots_size = 0.1;
          dots_spacing = 0.3;
          outer_color = "rgba(9a8d9555)";
          inner_color = "rgba(120f1111)";
          font_color = "rgba(d1c2cbff)";
          fade_on_empty = true;
          position = "0, 20";
          halign = "center";
          valign = "center";
        }
      ];

      label = [
        {
          # clock
          text = "$TIME";
          color = "rgba(eae0e4FF)";
          font_size = 65;
          position = "0, 300";
          halign = "center";
          valign = "center";
        }
      ];

    };
  };
}
