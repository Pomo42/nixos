{ pkgs, ... }:
{
  programs.zsh = {
    enable = true;
    syntaxHighlighting.enable = true;
    shellAliases = {
      ls = "${pkgs.eza}/bin/eza -lh --git";
      nb = "${pkgs.newsboat}/bin/newsboat";
      lg = "${pkgs.lazygit}/bin/lazygit";
      v = "nvim";
      cat = "${pkgs.bat}/bin/bat";
      ytv = "${pkgs.yt-dlp}/bin/yt-dlp -f mp4";
      yta = "${pkgs.yt-dlp}/bin/yt-dlp -f m4a";
    };

    defaultKeymap = "viins";

    history.size = 10000;
    history.path = "$HOME/.cache/zsh/history";

    initExtra = builtins.readFile ./zshrc;
  };

  home.file.".zprofile" = {
    enable = true;
    text = ''
      [ "$(tty)" = "/dev/tty1" ] && ${pkgs.xorg.xinit}/bin/startx ./.xinitrc
    '';
  };

}
