{ pkgs, ... }:
{
  home.packages = with pkgs; [
    cargo
    rustc
    python3
    gcc
  ];
  programs.neovim = {
    enable = true;
    defaultEditor = true;

    viAlias = true;
    vimAlias = true;
    vimdiffAlias = true;
    plugins = with pkgs.vimPlugins; [
      nerdtree
      nvim-lspconfig
      rust-tools-nvim
      nvim-treesitter.withAllGrammars
      vim-table-mode
    ];

    extraPackages = with pkgs; [
      # Rust
      rust-analyzer
      rustfmt
      # Nix
      nil
      nixfmt-rfc-style
      # Python
      black
      nodejs
    ];

    coc = {
      enable = true;
      settings = {
        languageserver = {
          nix = {
            command = "nil";
            filetypes = [ "nix" ];
            rootPatterns = [ "*.nix" ];
          };
        };
      };
    };

    # This gets rust lsp working
    extraLuaConfig = # lua
      ''
        --Treesitter

        require'nvim-treesitter.configs'.setup {
        	highlight = {
        		enable = true,
        	},
        }

        -- Rust LSP
        local rt = require("rust-tools")

        rt.setup({
          server = {
        	on_attach = function(_, bufnr)
        	  -- Hover actions
        	  vim.keymap.set("n", "<C-space>", rt.hover_actions.hover_actions, { buffer = bufnr })
        	  -- Code action groups
        	  vim.keymap.set("n", "<Leader>a", rt.code_action_group.code_action_group, { buffer = bufnr })
        	end,
          },
        })
      '';

    extraConfig = # vim
      ''
        let mapleader = " "

        "NERDTree
        nnoremap <C-f> :NERDTreeToggle<CR>
        autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

        "Sets
        set number relativenumber
        set scrolloff=8
        set ruler
        set tabstop=4
        set shiftwidth=4
        set ignorecase
        set smartcase
        set incsearch
        set smartindent
        set nowrap
        set nohlsearch
        set noswapfile

        "Mappings
        nnoremap <leader>o :setlocal spell! spelllang=en_gb<CR>
        nnoremap <C-h> <C-w>h
        nnoremap <C-j> <C-w>j
        nnoremap <C-k> <C-w>k
        nnoremap <C-l> <C-w>l
        nnoremap Y y$
        nnoremap <leader><leader> /<++><CR>ca<
        vnoremap <leader>yy :!xclip -f -sel clip<CR>

        ""Latex compiling
        nnoremap <leader>s :!pdflatex %<CR><CR>
        " PDF viewing
        nnoremap <leader>p :!zathura %:t:r.pdf &<CR><CR>

        ""Markdown tables
        autocmd BufEnter *.md TableModeEnable

        "" Only enable COC for certain filetypes
        function! s:enable_coc_for_type()
        	let l:filesuffix_whitelist = ['nix', 'py', 'rs']
        	if index(l:filesuffix_whitelist, expand('%:e')) == -1
            	let b:coc_enabled = 0
            endif
        endfunction
        autocmd BufRead,BufNewFile * call s:enable_coc_for_type()

        ""Autoformatters
        autocmd BufWritePost *.rs :!${pkgs.rustfmt}/bin/rustfmt % 
        autocmd BufWritePost *.nix :!${pkgs.nixfmt-rfc-style}/bin/nixfmt % 
        autocmd BufWritePost *.py :!${pkgs.black}/bin/black % 
      '';
  };
}
