{ ... }:
{
  imports = [
    ./alacritty.nix
    ./btop.nix
    ./firefox.nix
    #./hyprland.nix
    ./neovim.nix
    ./newsboat.nix
    ./obs.nix
    ./xinit.nix
    ./zsh.nix
  ];
}
