{ pkgs, inputs, ... }:
{
  programs.firefox = {
    enable = true;

    profiles.amy = {
      extensions = with inputs.firefox-addons.packages."x86_64-linux"; [
        ublock-origin
        youtube-shorts-block
        consent-o-matic
        libredirect
        awesome-rss
        vimium
      ];

      bookmarks = [
        {
          name = "Nix search";
          url = "https://search.nixos.org";
        }
        {
          name = "Youtube";
          url = "https://youtube.com";
        }
        {
          name = "Discord time conversion";
          url = "https://discordtimestamp.com";
        }
        {
          name = "Wikipedia";
          url = "https://en.wikipedia.org/wiki/Main_Page";
        }
        {
          name = "SOT wiki";
          url = "https://seaofthieves.wiki.gg/wiki/Sea_of_Thieves_Wiki";
        }
        {
          name = "Sea of Thieves";
          url = "https://www.seaofthieves.com/";
        }
      ];

      settings = {
        "dom.security.https_only_mode" = true;
      };

    };
  };
}
